<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produk;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class ProdukController extends Controller
{
    public function index()
    {
    	try {
    		$Produk = Produk::all();

    		$response=$Produk;
    		$code=200;

    	} catch (Exception $e) {
    		$code=500;
    		$response=$e->getMessage();
    	}

    	return apiResponseBuilder($code, $response);
    }

    public function store(Request $request)
    {
    	try {
    		$Produk = new Produk();
    		$Produk->nama = $request->nama;

    		$Produk->save();
    		$code=200;
    		$response=$Produk;
    		
    	} catch (Exception $e) {
    		if ($e instanceof ValidationException) {
    			$code=400;
    			$response= 'tidak ada data';
    		} else {
    			$code=500;
    			$response=$e->getMessage();
    		}
    	
    	}
    		return apiResponseBuilder($code, $response);
    }

    	public function show($id)
    	{
    		try {
    			$Produk = Produk::findOrfail($id);
    			$code=200;
    			$response=$Produk;
    		} catch (Exception $e) {
    			if ($e instanceof ModelNotFoundException) {
    				$code=404;
    				$response='inputkan data sesuai id';

    			} else {
    				$code=500;
    				$response=$e->getMessage();
    			}
    		}

    		return apiResponseBuilder($code, $response);
    	}
    	public function update(Request $request, $id)
    	{
    		try {
    			$Produk=Produk::find($id);

    			$Produk->nama = $request->nama;
    			$Produk->save();

    			$code=200;
    			$response=$Produk;
    		} catch (Exception $e) {
    			$code=500;
    			$response=$e->getMessage();
    		}
    		return apiResponseBuilder($code, $response);
    	}

    	public function destroy($id)
    	{
    		try {
    			$Produk=Produk::find($id);
    			$Produk->delete();
			    $code=200;
			    $response=$Produk;
    		} catch (Exception $e) {
    			$code=500;
			    $response=$e->getMessage();
    		}
    		return apiResponseBuilder($code, $response);

    	}
}
